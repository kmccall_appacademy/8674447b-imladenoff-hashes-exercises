# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = {}
  str.split.each do |word|
    words[word] = word.length
  end
  words
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.key(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k,v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  freq = Hash.new 0
  word.each_char { |char| freq[char] += 1 }
  freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  freq = Hash.new 0
  arr.each { |item| freq[item] += 1 }
  (freq.select { |k,v| v = 1}).keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  freq = Hash.new 0
  numbers.each do |number|
    if number.even?
      freq[:even] += 1
    else
      freq[:odd] += 1
    end
  end
    freq
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = Hash.new 0
  string.each_char { |char| vowels[char] += 1 if "aeiou".include? char }
  (vowels.sort.sort_by { |k,v| -v })[0][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second = students.select { |k,v| v > 6 }
  second_array = second.keys
  output = []
  second_array.each do |student1|
    second_array.each do |student2|
      output << [student1, student2].sort if student1 != student2
    end
  end
  output.uniq
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  diversity = Hash.new 0
  specimens.each { |specimen| diversity[specimen] += 1 }
  number_of_species = diversity.count
  smallest_population_size = diversity.values.min
  largest_population_size = diversity.values.max
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  vandalized_sign.each_char { |char| return false if character_count(vandalized_sign)[char] > character_count(normal_sign)[char] }
  true
end

def character_count(str)
  char_count = Hash.new 0
  str.each_char { |char| char_count[char.downcase] += 1 }
  char_count
end
